Introduction
============

The tools are separeted in two main parts:

Skimming
--------

Used to join the ntuples from a run when they are spread across many files into a single ROOT File. Normally runned at lxplus and the output file used to analysis made with the plots code.


Ramp Processing
---------------

Process the Ntuple generated by the skimming code and generate a .npy (Numpy) file where data analysis with python is possible


Plots
-----

Analysis of the Ntuple generated by the skimming code and print the linearity plots